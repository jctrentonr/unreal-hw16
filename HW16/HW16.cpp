#include <iostream>
#include <time.h>

#define _CRT_SECURE_NO_WARNINGS = 1;

int main()
{
	const int N = 8;

	int array[N][N] = { {} };

	// �������� �������
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			array[i][j] = i + j;
		}
	}

	// ����� �������
	std::cout << "Array:\n";
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			std::cout << array[i][j] << "\t";
		}
		std::cout << "\n";
	}


#pragma warning(disable:4996)
	// ����� ������� ������ �� �������
	time_t t = time(NULL);
	struct tm* timePtr = localtime(&t);
	int day = timePtr->tm_mday;

	int idx = day % N;
	int sum = 0;
	for (int i = 0; i < N; ++i)
	{
		sum += array[idx][i];
	}
	std::cout << "Sum: " << sum << ", of row with index " << idx;
}
